# Havadarya Port Strike #

Havadarya Port Strike, an Airgoons A-mission for use with flights up 20-30

## Objectives ##

Strike a pair of Iranian Mouge class destroyers (represented by Neutrashimmy), destroy chemical storage facilities within the port, and SEAD the area.

## TODO ##
* Scale back SAMs a bit.
* Replace trigger zones with scripting to account for flights moving ahead of the package.
* Maybe figure out a way to disable ships on hit?  DCS ships are too strong.