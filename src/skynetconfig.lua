iranIADS = SkynetIADS:create('Iran')
iranIADS:addSAMSitesByPrefix('IRAN-SAM')
iranIADS:addEarlyWarningRadarsByPrefix('IRAN-EWR')
iranIADS:addEarlyWarningRadar('IRAN-AWACS-A50-S')
iranIADS:addEarlyWarningRadar('Jamaran')
iranIADS:addEarlyWarningRadar('Damavand')

local commandCenter = Unit.getByName('IRAN-CC-IADS1')
iranIADS:addCommandCenter(commandCenter)

iranIADS:getSAMSitesByPrefix('IRAN-SAM-SA10'):setActAsEW(true):setAutonomousBehaviour(SkynetIADSAbstractRadarElement.AUTONOMOUS_STATE_DARK)
iranIADS:getSAMSitesByPrefix('IRAN-SAM-HK'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(102):setAutonomousBehaviour(SkynetIADSAbstractRadarElement.AUTONOMOUS_STATE_DARK)
iranIADS:getSAMSitesByPrefix('IRAN-SAM-SA6'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(105):setAutonomousBehaviour(SkynetIADSAbstractRadarElement.AUTONOMOUS_STATE_DARK)
iranIADS:getSAMSitesByPrefix('IRAN-SAM-SA15'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(105):setAutonomousBehaviour(SkynetIADSAbstractRadarElement.AUTONOMOUS_STATE_DARK)

local sa15 = iranIADS:getSAMSiteByGroupName('IRAN-SAM-SA15-ER06')
iranIADS:getSAMSiteByGroupName('IRAN-SAM-SA10-ER06'):addPointDefence(sa15)

local iranDebug = iranIADS:getDebugSettings()
--in game
iranDebug.IADSStatus = false
iranDebug.contacts = false
iranDebug.jammerProbability = false
--dcs.log
iranDebug.addedEWRadar = false
iranDebug.addedSAMSite = false
iranDebug.warnings = false
iranDebug.radarWentLive = false
iranDebug.radarWentDark = false
iranDebug.harmDefence = false
--detailed logging
iranDebug.samSiteStatusEnvOutput = false
iranDebug.earlyWarningRadarStatusEnvOutput = false
iranDebug.commandCenterStatusEnvOutput = false

iranIADS:setupSAMSitesAndThenActivate()

